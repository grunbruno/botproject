
package com.vivo.rest;
import com.google.gson.Gson;
import com.vivo.dao.*;
import com.vivo.model.Bot;
import com.vivo.model.Message;
import java.util.ArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/** REST para gerenciamento das conversas e mensagens entre bots e clientes 
* @author Grun
* @version 1.0
* @since Release 01 da aplicação
*/
@Path("/messages")
public class MessageRest {
    
    /** Método para criar uma mensagem
    * @return String - true or false*/
    @Path("/create")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public String createMessage(Message messageS){
        MessageDAO mdao = MessageDAOFactory.getMessageDAO();
        boolean res = mdao.createMessage(messageS);
        Gson gson = new Gson();		
        String json = gson.toJson(res);

        return json;
    }
    
    /** Método para buscar as mensagens de uma conversa entre um determinado bot e um cliente
    * @return String - todas as mensagens trocadas*/
    @Path("/conversation/{conversation-id}")
    @GET
    @Produces("application/json")
    public String getMessagesByConversation(@PathParam("conversation-id") long id){
        MessageDAO mdao = MessageDAOFactory.getMessageDAO();
        ArrayList<Message> mList = mdao.getMessagesByConversationID(id);
        Gson gson = new Gson();
        return  gson.toJson(mList);
    }
    
    /** Método para buscar uma mensagem mensagens
    * @return String - a mensagem*/
    @Path("/{message-id}")
    @GET
    @Produces("application/json")
    public String getMessageById(@PathParam("message-id") long id){
        MessageDAO mdao = MessageDAOFactory.getMessageDAO();
        Message msg = mdao.getMessageByID(id);
        Gson gson = new Gson();
        return  gson.toJson(msg);
    }
}
