
package com.vivo.rest;
import com.google.gson.Gson;
import com.vivo.dao.*;
import com.vivo.model.Bot;
import java.util.ArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/** REST para gerenciamento dos bots (CRUD) 
* @author Grun
* @version 1.0
* @since Release 01 da aplicação
*/
@Path("/bots")
public class BotRest {
    
    /** Método para listar todos os bots
    * @return String */
    @GET
    @Produces("application/json")
    public String getBots(){
        BotDAO bdao = BotDAOFactory.getBotDAO();
        ArrayList<Bot> list = bdao.getBots();
        Gson gson = new Gson();
        return gson.toJson(list);
    }
    
    /** Método para buscar um determinado bot
    * @return String - o bot*/
    @Path("{bot-id}")
    @GET
    @Produces("application/json")
    public String getBot(@PathParam("bot-id") long id){
        BotDAO bdao = BotDAOFactory.getBotDAO();
        Gson gson = new Gson();
        Bot bot = bdao.getBOT(id);
        bot.setBoTID(id);
        return  gson.toJson(bot);
    }
    
    /** Método para criar um bot
    * @return String - true or false*/
    @Path("/create")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public String createBot(Bot botS){
        BotDAO bdao = BotDAOFactory.getBotDAO();
        boolean res = bdao.createBot(botS);
        Gson gson = new Gson();		
        String json = gson.toJson(res);

        return json;
    }
    
    /** Método para remover um bot
    * @return String - true or false*/
    @Path("/delete/{idDel}")
    @DELETE
    @Produces("application/json")
    @Consumes("application/json")
    public String deleteBot(@PathParam("idDel") Long botID){
        BotDAO bdao = BotDAOFactory.getBotDAO();
        boolean ret = bdao.deleteBot(botID);
        Gson gson = new Gson();		
        String json = gson.toJson(ret);

        return json;
    }
       
   /** Método para atualizar um bot
    * @return String - true or false*/     
    @Path("/update")
    @PUT
    @Produces("application/json")
    @Consumes("application/json")
    public String updateBot(Bot bot){
        BotDAO bdao = BotDAOFactory.getBotDAO();
        boolean ret = bdao.updateBot(bot);
        Gson gson = new Gson();		
        String json = gson.toJson(ret);

        return json;
    }
    
    
}


