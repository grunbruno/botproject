/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vivo.connections;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;

/**
 *
 * @author Grun
 */
public class MongoConnection {
    private DB banco = null;
       
    public MongoConnection() {
    }
        
    public DB conectaMongoDB(){
        
        try {
            Mongo mongoCli;
            mongoCli = new MongoClient("127.0.0.1");
            banco = mongoCli.getDB("vivo_db");
            
        
        } catch (Exception ex)	{
            System.out.println("MongoException	:" +ex.getMessage());
        }
        
       return banco; 
    }
}
