/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vivo.dao;

import com.vivo.model.Message;
import java.util.ArrayList;

/**
 *
 * @author Grun
 */
public interface MessageDAOI {
    
    public boolean createMessage(Message message);
    public ArrayList<Message> getMessagesByConversationID(long conversationID);
    public Message getMessageByID(long idMessage);
    
}
