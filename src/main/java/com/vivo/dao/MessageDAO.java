/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vivo.dao;

import com.vivo.model.Message;
import java.util.ArrayList;
import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;
import com.vivo.connections.MongoConnection;
import com.vivo.model.Bot;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Grun
 */
public class MessageDAO implements MessageDAOI{

    @Override
    public boolean createMessage(Message message) {
        WriteResult wr = null;
        MongoConnection mongo;
        DB banco;
        DBCollection collection;
        mongo = new MongoConnection();
        banco = mongo.conectaMongoDB();
        collection = banco.getCollection("message");
        BasicDBObject index = new BasicDBObject("messageID",1);
        collection.ensureIndex(index,new BasicDBObject("unique",true));
        BasicDBObject obj = new	BasicDBObject();
                
        obj.put("messageID",message.getMessageID());
        obj.put("conversationId",message.getConversationId());
        obj.put("from",message.getFrom());
        obj.put("to",message.getTo());
        obj.put("timestamp",message.getTimestamp().toString());
        obj.put("text",message.getText());
        obj.put("owner",message.getOwner().toString());
        
        try {
            wr = collection.insert(obj);
        } catch (Exception e){
            System.out.println("Erro de conexão:"+e.getMessage());
        }
        return wr != null;
               
    }

    @Override
    public ArrayList<Message> getMessagesByConversationID(long conversationID) {
        ArrayList<Message> mList = new ArrayList<Message>();
        Message message = null;         
        MongoConnection mongo;
        DB banco;
        DBCollection collection;
        
        mongo = new MongoConnection();
        banco = mongo.conectaMongoDB();
        collection = banco.getCollection("message");
        
        BasicDBObject query = new BasicDBObject();
        query.put("conversationId", conversationID);
        DBCursor cursor = collection.find(query);
        
        Gson gson=new Gson();
        
        try {
            while(cursor.hasNext()) {
                
                DBObject obj = cursor.next();
                System.out.println(obj.toString());
                message = gson.fromJson(obj.toString(), Message.class);
                
                if(message != null){
                    mList.add(message);
                }
                
            }
        } finally {
            cursor.close();
        }
        
        return mList;
    }

    @Override
    public Message getMessageByID(long messageID) {
        Message msg = null;         
        MongoConnection mongo;
        DB banco;
        DBCollection collection;
        
        mongo = new MongoConnection();
        banco = mongo.conectaMongoDB();
        collection = banco.getCollection("message");
        
        BasicDBObject query = new BasicDBObject();
        query.put("messageID", messageID);
        DBCursor cursor = collection.find(query);
        Gson gson=new Gson();
        
        while (cursor.hasNext()) {
            DBObject obj = cursor.next();
            msg = gson.fromJson(obj.toString(), Message.class);
	}
        
        return msg;
    }
    
}
