/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vivo.dao;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;
import com.vivo.connections.MongoConnection;
import com.vivo.model.Bot;
import java.util.ArrayList;
import java.util.Random;


/**
 *
 * @author Grun
 */
public class BotDAO implements BotDAOI{
       
    @Override
    public Bot getBOT(long id) {
        Bot bot = null;         
        MongoConnection mongo;
        DB banco;
        DBCollection collection;
        
        mongo = new MongoConnection();
        banco = mongo.conectaMongoDB();
        collection = banco.getCollection("bot");
        
        BasicDBObject query = new BasicDBObject();
        query.put("botID", id);
        DBCursor cursor = collection.find(query);
        Gson gson=new Gson();
        
        while (cursor.hasNext()) {
            DBObject obj = cursor.next();
            bot = gson.fromJson(obj.toString(), Bot.class);
	}
        
        return bot;
    }

    @Override
    public ArrayList<Bot> getBots() {
        
        ArrayList<Bot> botList = new ArrayList<Bot>();
        Bot bot = null;         
        MongoConnection mongo;
        DB banco;
        DBCollection collection;
        
        mongo = new MongoConnection();
        banco = mongo.conectaMongoDB();
        collection = banco.getCollection("bot");
        
        DBCursor cursor = collection.find();
        
        Gson gson=new Gson();
        
        try {
            while(cursor.hasNext()) {
                
                DBObject obj = cursor.next();
                System.out.println(obj.toString());
                bot = gson.fromJson(obj.toString(), Bot.class);
                
                if(bot != null){
                    botList.add(bot);
                }
                
            }
        } finally {
            cursor.close();
        }
        
        return botList;
    }

    @Override
    public boolean createBot(Bot bot) {
        WriteResult wr = null;
        MongoConnection mongo;
        DB banco;
        DBCollection collection;
        mongo = new MongoConnection();
        banco = mongo.conectaMongoDB();
        collection = banco.getCollection("bot");
        BasicDBObject index = new BasicDBObject("botID",1);
        collection.ensureIndex(index,new BasicDBObject("unique",true));
        BasicDBObject obj = new	BasicDBObject();
                
        obj.put("botID",bot.getBotID());
        obj.put("name",bot.getName());
        
        try {
           wr = collection.insert(obj);
        } catch (Exception e){
            System.out.println("Erro de conexão:"+e.getMessage());
        }
        System.out.println("REsulado: "+wr);
        return wr != null;
    }
    

    @Override
    public boolean updateBot(Bot bot) {
        MongoConnection mongo;
        DB banco;
        DBCollection collection;
        mongo = new MongoConnection();
        banco = mongo.conectaMongoDB();
        collection = banco.getCollection("bot");
        BasicDBObject newDocument = new BasicDBObject();
	newDocument.append("$set", new BasicDBObject().append("name", bot.getName()));
			
	BasicDBObject searchQuery = new BasicDBObject().append("botID", bot.getBotID());

	WriteResult wr = collection.update(searchQuery, newDocument);
                      
        return wr.getN() != 0;
    }

    @Override
    public boolean deleteBot(Long botID) {
        MongoConnection mongo;
        DB banco;
        DBCollection collection;
        mongo = new MongoConnection();
        banco = mongo.conectaMongoDB();
        collection = banco.getCollection("bot");
        BasicDBObject document = new BasicDBObject();
	document.put("botID", botID);
	WriteResult wr = collection.remove(document);
        
        System.out.println("Deleteeee "+wr.getN());
        return wr.getN() != 0;
    }
    
}
