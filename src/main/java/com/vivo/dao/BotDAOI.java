/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vivo.dao;
import com.vivo.model.Bot;
import java.util.ArrayList;

/**
 *
 * @author Grun
 */
public interface BotDAOI {
    public Bot getBOT(long id);
    public ArrayList<Bot> getBots();
    public boolean createBot(Bot bot);
    public boolean updateBot(Bot bot);
    public boolean deleteBot(Long botID);
    
    
}
