/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vivo.dao;

/**
 *
 * @author Grun
 */
public final class BotDAOFactory {
    private BotDAOFactory(){
    }
    
    public static BotDAO getBotDAO(){
        return new BotDAO();
    }
}
