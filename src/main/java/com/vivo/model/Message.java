/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vivo.model;



/**
 *
 * @author Grun
 */
public class Message {
    private long messageID;
    private long conversationId;
    private long from;
    private long to;
    private String timestamp;
    private String text;
    private Owner owner; // remetente da mensagem
    
    public enum Owner{
    	BOT,USER
    }
    
    public Message() {
    }

    public long getMessageID() {
        return messageID;
    }

    public void setMessageID(long messageID) {
        this.messageID = messageID;
    }
    
    
    public long getConversationId() {
        return conversationId;
    }

    public long getFrom() {
        return from;
    }

    public long getTo() {
        return to;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getText() {
        return text;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setConversationId(long conversationId) {
        this.conversationId = conversationId;
    }

    public void setFrom(long from) {
        this.from = from;
    }

    public void setTo(long to) {
        this.to = to;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }
    
    @Override
    public String toString(){
        return "Objeto Mensagem: MesID "+this.messageID+" ConID "+this.conversationId+" From "+this.from+
                " To "+this.to+" Own "+this.owner+" Tms "+this.timestamp+" Txt"+this.text;
    }
    
    
    
}
