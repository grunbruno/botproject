/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vivo.model;

/**
 *
 * @author Grun
 */
public class Bot {
    private Long botID;
    private String name;

    public Bot() {
    }

    public void setBoTID(Long botID) {
        this.botID = botID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getBotID() {
        return botID;
    }

    public String getName() {
        return name;
    }
    
    @Override
    public String toString(){
        return "ID: "+this.botID+" Nome: "+this.name;
    }
    
    
}
